;; Minimal UI
(scroll-bar-mode -1)
(tool-bar-mode   -1)
(tooltip-mode    -1)
(menu-bar-mode   -1)

(add-to-list 'default-frame-alist '(font . "mononoki-12"))
(add-to-list 'default-frame-alist '(height . 24))
(add-to-list 'default-frame-alist '(width . 80))





;; straight.el init
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))



(straight-use-package 'use-package)


;; Keep packages up to date
(use-package auto-package-update
  :straight t
  :config
  (setq auto-package-update-delete-old-versions t)
  (setq auto-package-update-hide-results t)
  (auto-package-update-maybe))

;; Theme
(use-package doom-themes
  
  :straight t
  :config
  (load-theme 'doom-one t))

;; Helm
(use-package helm
  
  :straight t
  :init
  (setq helm-M-x-fuzzy-match t
  helm-mode-fuzzy-match t
  helm-buffers-fuzzy-matching t
  helm-recentf-fuzzy-match t
  helm-locate-fuzzy-match t
  helm-semantic-fuzzy-match t
  helm-imenu-fuzzy-match t
  helm-completion-in-region-fuzzy-match t
  helm-candidate-number-list 150
  helm-split-window-inside-p t
  helm-move-to-line-cycle-in-source t
  helm-echo-input-in-header-line t
  helm-autoresize-max-height 0
  helm-autoresize-min-height 20)
  :config
  (helm-mode 1))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (paradox auto-package-update docker dockerfile-mode elixir-mode undo-tree multiple-cursors flycheck markdown-mode yaml-mode protobuf-mode json-mode web-mode fish-mode rainbow-mode flx-ido dash-at-point dash magit company eglot neotree projectile which-key helm doom-themes use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Ido, Yes!
(use-package ido
  :straight t
  :config
  (setq ido-enable-flex-matching t)
  (ido-mode t))


;; Save point position between sessions.
(use-package saveplace
  :straight t
  :config
  (setq-default save-place t)
  (setq save-place-file (expand-file-name "places" user-emacs-directory)))

;; Which Key
(use-package which-key
  
  :straight t
  :init
  (setq which-key-separator " ")
  (setq which-key-prefix-prefix "+")
  :config
  (which-key-mode 1))

(use-package flx-ido
  
  :straight t
  :init)

;; Projectile
(use-package projectile
  
  :straight t
  :init
  (setq projectile-require-project-root nil)
  :config
  (projectile-mode +1)
  (setq projectile-project-search-path '("~/git"))
  :bind-keymap
  ("C-c p" .  projectile-command-map))


;; All The Icons
(use-package all-the-icons 
  :straight t)

;; neotree
(use-package neotree
  
  :straight t
  :init
  :config
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  (setq neo-smart-open t)
  )

(global-set-key [f8] 'neotree-toggle)

(use-package eglot
  
  :straight t
  :init
  :config
  (add-to-list 'eglot-server-programs '(elixir-mode . ("~/git/elixir-ls/release/language_server.sh")))
  (add-hook 'elixir-mode 'eglot-ensure))


;; SHOW MATCHING PARENS
(setq show-paren-delay 0)
(show-paren-mode 1)

(use-package company
  
  :straight t
  :init
  (add-hook 'after-init-hook 'global-company-mode)  
  )

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(use-package magit
  
  :straight t
  :commands (magit-section-toggle)
  :diminish magit-auto-revert-mode
  :bind ( ("C-x g" . magit-status)
	 (:map magit-mode-map
              ("<tab>" . magit-section-toggle))))

(use-package ediff
  :straight t
  :config
  (setq ediff-split-window-function 'split-window-horizontally)
  (setq ediff-window-setup-function 'ediff-setup-windows-plain))

(use-package fish-mode
  
  :straight t)



(use-package rainbow-mode
  
  :straight t
  ;; :diminish ""
  :hook ((web-mode . rainbow-mode)
         (css-mode . rainbow-mode)))

(defun my/move-line-up ()
  (interactive)
  (transpose-lines 1)
  (previous-line 2))

(defun my/move-line-down ()
  (interactive)
  (next-line 1)
  (transpose-lines 1)
  (previous-line 1))

(bind-key "<C-S-up>" 'my/move-line-up)
(bind-key "<C-S-down>" 'my/move-line-down)

(bind-key "M-o" 'other-window)
(bind-key "M-p" 'previous-buffer)
(bind-key "M-n" 'next-buffer)

;; Answering just 'y' or 'n' will do
(defalias 'yes-or-no-p 'y-or-n-p)

;; Auto-close brackets and double quotes
(electric-pair-mode 1)

;; Show matching parens
(setq show-paren-delay 0)
(show-paren-mode 1)


;; Full path in frame title
(when window-system
  (setq frame-title-format '(buffer-file-name "%f" ("%b"))))

;; Don't automatically copy selected text
(setq select-enable-primary nil)

;; eval-expression-print-level needs to be set to nil (turned off) so
;; that you can always see what's happening.
(setq eval-expression-print-level nil)





(use-package org

  
  :straight t
  :commands (org-babel-do-load-languages org-demote-subtree org-promote-subtree)
  :bind(:map org-mode-map
             ("<M-right>" . org-demote-subtree)
             ("<M-left>" . org-promote-subtree))
  :config
  ;; Essential Settings
  (setq org-src-fontify-natively t)
  (setq org-log-done 'time)
  (setq org-html-doctype "html5")
  (setq org-export-headline-levels 6)
  (setq org-export-with-smart-quotes t)

  ;; Custom TODO keywords
  (setq org-todo-keywords
        '((sequence "TODO(t)" "NOW(n@/!)" "|" "DONE(d!)" "CANCELED(c@)")))

  ;; setup org-capture
  ;; `M-x org-capture' to add notes. `C-u M-x org-capture' to visit file
  (setq org-capture-templates
        `(("t" "Tasks" entry (file+headline ,(concat org-directory "/todo.org") "Tasks")
           "* TODO %?\n %U\n  %i\n  %a")
          ("n" "Notes" entry (file ,(concat org-directory "/notes.org"))
           "* %?\n %i\n")))

  ;; Set up babel source-block execution
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (haskell . t)
     (C . t)))

  ;; Set up latex
  (setq org-export-with-LaTeX-fragments t)
  (setq org-preview-latex-default-process 'imagemagick)

  ;; local variable for keeping track of pdf-process options
  (setq pdf-processp nil)

  ;; Prevent Weird LaTeX class issue
  (unless (boundp 'org-latex-classes)
    (setq org-latex-classes nil))
  (add-to-list 'org-latex-classes
               '("per-file-class"
                 "\\documentclass{article}
                      [NO-DEFAULT-PACKAGES]
                      [EXTRA]"))

  (defun toggle-org-latex-pdf-process ()
    "Change org-latex-pdf-process variable.

    Toggle from using latexmk or pdflatex. LaTeX-Mk handles BibTeX,
    but opens a new PDF every-time."
    (interactive)
    (if pdf-processp
        ;; LaTeX-Mk for BibTex
        (progn
          (setq pdf-processp nil)
          (setq org-latex-pdf-process
                '("latexmk -pdflatex='pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f' -gg -pdf -bibtex-cond -f %f"))
          (message "org-latex-pdf-process: latexmk"))
      ;; Plain LaTeX export
      (progn
        (setq pdf-processp t)
        (setq org-latex-pdf-process
              '("xelatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
        (message "org-latex-pdf-process: xelatex")))))

(use-package web-mode
  
  :straight t
  :mode ("\\.html?" "\\.php\\'" "\\.vue\\'")
  :config
  (add-to-list 'web-mode-comment-formats '("javascript" . "//"))
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-style-padding 0)
  (setq web-mode-script-padding 0))

(use-package css-mode
  :straight t
  :mode ("\\css\\'" "\\.scss\\'" "\\.sass\\'")
  :config
  (setq css-indent-offset 2))

(use-package json-mode
  
  :straight t)

(use-package protobuf-mode
  
  :straight t)

(use-package yaml-mode
  
  :straight t)

(use-package markdown-mode
  
  :straight t)



(use-package flycheck
  
  :straight t
  :commands (flycheck-add-mode)
  :init
  (global-flycheck-mode 1)
  ;; :diminish ""
  :config
  (setq flycheck-indication-mode 'left-fringe)
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc )))

(use-package multiple-cursors
  
  :straight t
  :commands (set-rectangular-region-anchor)
  :bind (("C-c C-SPC" . set-rectangular-region-anchor)))

(use-package undo-tree
  
  :straight t
  :init
  (global-undo-tree-mode)
  (setq undo-tree-visualizer-timestamps t)
  (setq undo-tree-visualizer-diff t))


;; make ctrl-z undo
(global-set-key (kbd "C-z") 'undo)
;; make ctrl-Z redo
(defalias 'redo 'undo-tree-redo)
(global-set-key  (kbd "C-Z")  'redo)

(use-package elixir-mode
  
  :straight t
  :init)

(use-package docker
  
  :straight t
  :init
  :bind
  ("C-c d" . docker)
  :custom
  (docker-command "podman"))

(use-package dockerfile-mode
  
  :straight (dockerfile-mode :type git :host github :repo "spotify/dockerfile-mode"
			     :fork (:host github
					  :repo "MaSven/dockerfile-mode"))
  :custom
  (dockerfile-mode-command "podman"))
